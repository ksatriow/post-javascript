// Jawaban soal no 3a
function hitungLuasSegitiga() {
  var alasSegitiga = prompt("Masukkan alas segitiga");
  var tinggiSegitiga = prompt("Masukkan tinggi segitga");
  var hasilLuasSegitiga = 0.5 * alasSegitiga * tinggiSegitiga;

  document.getElementById("segitiga").value = hasilLuasSegitiga;
}

// Jawaban soal no 3b
function hitungLuasLingkaran() {
  let jari = prompt("Masukan nilai jari - jari");
  let hasilLuasLingkaran = Math.PI * jari * jari;
  document.getElementById("lingkaran").value = hasilLuasLingkaran;
}

// Jawaban soal no 3c
let kabupatenTrenggalek = {
  julukan: "Minak Sopal",
  provinsi: "Jawa Timur",
  moto: "Jwalita Praja Karana",
  bupati: "Mochammad Nur Arifin",
  jumlahPenduduk: 700000,
};
console.log(kabupatenTrenggalek);
console.info(`Julukan : ${kabupatenTrenggalek.julukan}`);
console.info(`Provinsi : ${kabupatenTrenggalek.provinsi}`);
console.info(`Moto : ${kabupatenTrenggalek.moto}`);
console.info(`Bupati : ${kabupatenTrenggalek.bupati}`);
console.info(`Penduduk : ${kabupatenTrenggalek.jumlahPenduduk}`);
console.table(kabupatenTrenggalek);

// Jawaban soal no 3d
let arrayAsean = [
  "Indonesia",
  "Malaysia",
  "Brunei Darussalam",
  "Singapore",
  "Laos",
  "Kamboja",
  "Philipina",
  "Thailand",
  "Vietnam"
];
arrayAsean.forEach((negara) => console.log(negara));
